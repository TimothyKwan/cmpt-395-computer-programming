package com.example.myapplication;

/*
* update log: 10/12/2020
* Timothy Kwan
* updated the constructor
* */
public class ContactInfo {
    private String cel;
    private String email;

    public ContactInfo(String phone, String email) {
        this.cel = phone;
        this.email = email;
    }

    public String toString() {
        return "Cel: " + cel +
                "\nEmail: " + email;
    }

    public String getCel() {
        return this.cel;
    }

    public void setCel(String newCel) {
        this.cel = newCel;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }
}
