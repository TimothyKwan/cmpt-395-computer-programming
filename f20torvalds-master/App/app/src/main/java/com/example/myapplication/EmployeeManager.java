package com.example.myapplication;


import android.content.Context;

import java.util.ArrayList;
import java.util.List;

//purpose: Manage a list of Employee
//Author: Timothy Kwan
//Date: 10/3/2020
/*
* update log: 10/12/2020
* Timothy Kwan
* Updated the AddEmployee and RemoveEmployee so it adds and removes the employee in the database
* added the initialize method and a boolean to check whether the table is initialized once the getInsteance is called
* */
public class EmployeeManager {
    private boolean isInitialized = false;
    private List<Employee> employees = new ArrayList<>();
    //save the database name
    private String employee_db = "Employee.db";
    private EmployeeDBHandler dbHandler;
    private ShiftManager shiftManager = ShiftManager.getInstance();


    //start of singleton
    private static EmployeeManager instance = new EmployeeManager();
    private EmployeeManager(){}

    public static EmployeeManager getInstance(){
        return instance;
    }

    public List<Employee> getEmployees(){
        return employees;
    }
    //end of singleton

    //purpose: initialize the manager and set up the database handler
    public void Initialize(Context context){
        if(!isInitialized){
            dbHandler = new EmployeeDBHandler(context, employee_db, null, 3);
            shiftManager.Initialization(context);
            //Get all the employee info from database
            employees = dbHandler.getAllEmployee_db();
            for (int i = 0; i< employees.size();i++){
                employees.get(i).initializeEmployeeShifts(
                        shiftManager.getAvailableShifts(employees.get(i).getEmployeeName()),
                        shiftManager.getEmployeeShifts(employees.get(i).getEmployeeName())
                        );
            }
            isInitialized = true;
        }
    }

    public long AddEmployee(Employee e){
        employees.add(e);
        return dbHandler.addItem(e);
    }

    public void RemoveEmployee(Employee e){
        if(employees.size()>0){
            /*if there's an employee, then:
            * 1. remove all the shifts related to the employee
            * 2. remove the employee in the database
            * 3. remove the employee from the employee list
            */
            shiftManager.removeAllEmployeeShifts(e.getEmployeeName());
            dbHandler.removeItem(e);
            employees.remove(e);
        }
    }

    public void RemoveEmployee(int employeeIndex){
        if(employeeIndex >= 0){
            /*if there's an employee, then:
             * 1. remove all the shifts related to the employee
             * 2. remove the employee in the database
             * 3. remove the employee from the employee list
             */
            shiftManager.removeAllEmployeeShifts(employees.get(employeeIndex).getEmployeeName());
            dbHandler.removeItem(employees.get(employeeIndex));
            employees.remove(employeeIndex);
        }
    }
    public int EditEmployee(Employee e){
        //TODO:
        //  add edit method when the employee changes name
        shiftManager.editAllEmployeeShifts(e.getEmployeeName());
        return dbHandler.editItem(e);
    }
}