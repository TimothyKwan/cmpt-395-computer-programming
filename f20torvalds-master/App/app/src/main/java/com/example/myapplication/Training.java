package com.example.myapplication;
/*
* update log: 10/12/2020
* Timothy Kwan
* updated the constructor
* */
public class Training {
    private Boolean trainedToOpen;
    private Boolean trainedToClose;

    // Default training is set to false, because new hires do not have training
    public Training(Boolean trainedToOpen, Boolean trainedToClose) {
        this.trainedToOpen = trainedToOpen;
        this.trainedToClose = trainedToClose;
    }

    public String toString() {
        String training = "";
        if (this.trainedToOpen) {
            training += "Opening Training: Completed\n";
        } else {
            training += "Opening Training: Not completed\n";
        }
        if (this.trainedToClose) {
            training += "Closing Training: Completed";
        } else {
            training += "Closing Training: Not completed";
        }
        return training;
    }

    public Boolean getTrainedToOpen() {
        return this.trainedToOpen;
    }

    public void setTrainedToOpen(Boolean training) {
        this.trainedToOpen = training;
    }

    public Boolean getTrainedToClose() {
        return this.trainedToClose;
    }

    public void setTrainedToClose(Boolean training) {
        this.trainedToClose = training;
    }
}