package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/*
 * purpose: This class handles busy day. It add, remove, and get all of the busy date
 * Author: Timothy Kwan
 * date: 11/28/2020
 *
 * Private variable
 * BUSY_DAY_TABLE: store the busy day table name
 * BUSY_DAY_ID: store the row ID of busy day table
 * BUSY_DAY_DATE: store the busy day date
 * */
public class BusyDayDBHandler extends SQLiteOpenHelper implements DataBaseHandler {
    private static final String BUSY_DAY_TABLE = " BUSY_DAY_TABLE ";
    private static final String BUSY_DAY_ID = " ID ";
    private static final String BUSY_DAY_DATE = " BUSY_DATE ";

    public BusyDayDBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {

        super(context, name, factory, version);
    }

    /*
     * Author: Timothy Kwan
     * purpose: called when the object is created
     * Date: 11/28/2020
     * */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableStatement = " CREATE TABLE "+ BUSY_DAY_TABLE + " ( "+BUSY_DAY_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                BUSY_DAY_DATE+" TEXT ) ";
        db.execSQL(createTableStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*
     * Author: Timothy Kwan
     * purpose: get all the busy date and return it into a list of string
     * Date: 11/28/2020
     * */
    public List<String> getAllBusyDate(){
        List<String> referenceList = new ArrayList<>();
        String queryString = " SELECT * FROM "+BUSY_DAY_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString,null);
        if(cursor.moveToFirst()){
            do{
                String date = cursor.getString(1);
                Log.i("BusyDae:", date.toString());
                referenceList.add(date);
            }
            while(cursor.moveToNext());
        }

        return referenceList;
    }

    /*
     * Author: Timothy Kwan
     * purpose: add the given string into the database table
     * Date: 11/28/2020
     * */
    public long addDate(String date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(BUSY_DAY_DATE, date);
        long returnMessage = db.insert(BUSY_DAY_TABLE, null, cv);
        return returnMessage;
    }

    /*
     * Author: Timothy Kwan
     * purpose: remove the busy day based on the given day
     * Date: 11/28/2020
     * */
    public boolean removeBusyDate(String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String queryString = " DELETE FROM "+BUSY_DAY_TABLE+" WHERE "+BUSY_DAY_DATE+" = "+" "+date+" ";
        Cursor cursor = db.rawQuery(queryString, null);
        return cursor.moveToFirst();
    }
}
