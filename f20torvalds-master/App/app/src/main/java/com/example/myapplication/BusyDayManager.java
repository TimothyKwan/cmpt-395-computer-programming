package com.example.myapplication;

import android.content.Context;
import android.os.Build;
import android.text.format.DateFormat;
import android.util.Log;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.Format;
import java.util.Locale;

/*
* Change: 11/12/2020
* Class name changed to busyDayManager for consistent naming scheme
* */
public class BusyDayManager {


    private List<String> busyDays = new ArrayList<>();
    private String busyDay_db = "busyDay_db";
    private BusyDayDBHandler dbHandler;
    private boolean isInitialized = false;
    //get the references of the managers
    private EmployeeManager employeeManager = EmployeeManager.getInstance();
    private ShiftManager shiftManager = ShiftManager.getInstance();

    //start of singleton
    private static BusyDayManager instance = new BusyDayManager();
    private BusyDayManager(){}

    public static BusyDayManager getInstance(){
        return instance;
    }

    //end of singleton

    public void initialize(Context context){
        if(!isInitialized){
            dbHandler = new BusyDayDBHandler(context, busyDay_db, null, 1);
            busyDays = dbHandler.getAllBusyDate();
            isInitialized = true;
        }
    }

    // must accept a string that is formatted using
    // public SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    // returns true if the matching day is found
    public boolean isBusy(String day){
        for(int i = 0; i < busyDays.size(); i ++){
            if(busyDays.get(i).equals(day)){
                return true;
            }
        }
        return false;
    }

    // must accept a string that is formatted using
    // public SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    public void addBusyDay(String day) throws ParseException {
        if(!isBusy(day)){
            busyDays.add(day);
            dbHandler.addDate(day);
            updateShifts(day);
        }
    }

    public void removeBusyDay(String day){
        if(isBusy(day)){
            busyDays.remove(day);
            dbHandler.removeBusyDate(day);
        }
    }

    //helper method to update the Shifts once the day is changed into a busy day
    private void updateShifts(String day) throws ParseException {
        //look for the all the shift based on the given day
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.CANADA);
        Date newDay = formatter.parse(day);

        List<Shift> availableShift = shiftManager.getShiftByDate(newDay, true);
        List<Shift> scheduled = shiftManager.getShiftByDate(newDay, false);
        Log.i("availableShift:", String.valueOf(availableShift.size()));
        for(int i = 0;i<availableShift.size();i++){
            //change all the existing shifts into the fullday
            availableShift.get(i).setAvailability(Shift.ShiftType.FullDay);
            //update the shifts in the database
            shiftManager.editShift(availableShift.get(i));
        }
        for(int i =0;i<scheduled.size();i++){
            scheduled.get(i).setAvailability(Shift.ShiftType.FullDay);
            shiftManager.editShift(scheduled.get(i));
        }

        List<Employee> employees = employeeManager.getEmployees();
        for(int i = 0;i<employees.size();i++){
            //update all the employee's shifts

            employees.get(i).initializeEmployeeShifts(shiftManager.getAvailableShifts(employees.get(i).getEmployeeName()),
                    shiftManager.getEmployeeShifts(employees.get(i).getEmployeeName()));
        }

    }
}
