package com.example.myapplication;

// Author: Kierra Andruko
// Date: 10/7/2020
/*=====================================
* update log: 10/12/2020
* Timothy Kwan
* updated the class constructor and a few get, set methods
* ======================================
* update log: 11/22/2020
* Timothy Kwan
* update add/remove ScheduledShift and add/remove AvailableShift
* ======================================
* update log: 11/27/2020
* Kierra Andruko
* Added the isPriority method for the schedule builder.
* */

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/*
 * Employee_id: is for the database
 * employeeName: stores the name of the employee
 * availability: stores the employees availability for potential shifts
 * scheduled: stores the employees scheduled shifts
 * training: stores boolean value if the employee has training for morning, afternoon, or both
 * contactInfo: stores the employees contact info (for irl manager use)
 * shiftManager: for the database
 */
public class Employee {

    private int Employee_id;//this is for the database
    private String employeeName;
    private List<Shift> availability = new ArrayList<>();
    private List<Shift> scheduled = new ArrayList<>();
    private Training training;
    private ContactInfo contactInfo;
    private ShiftManager shiftManager;

    public Employee(String employeeName, Training training, ContactInfo contact) {
        this.employeeName = employeeName;
        this.training = training;
        this.contactInfo = contact;
        shiftManager = ShiftManager.getInstance();
    }

    public String toString() {
        return "Employee Name: " + this.employeeName +
                "\nContact info: " + this.contactInfo +
                "\nTraining: " + this.training +
                "\nAvailability: " + this.availability +
                "\nSchedule: " + this.scheduled;
    }

    // Does not compare training - in case the manager wants to schedule this employee for training
    public Boolean isAvailableFor(Shift s) {
        for (int i = 0; i < availability.size(); i++) {
            if (s.isEqual(availability.get(i))) {
                return true;
            }
        }
        return false;
    }

    public void addAvailableShift(Shift s) {
        //shiftManager.addShift(s);
        this.availability.add(s);
    }

    public Boolean removeAvailableShift(Shift s) {
        //shiftManager.removeShift(s);
        return this.availability.remove(s);
    }

    public Boolean addScheduledShift(Shift s) {
        Log.i("Location: ", "inside addScheduledShift");
        //shiftManager.addShift(s);
        return this.scheduled.add(s);
    }

    public Boolean removeScheduledShift(Shift s) {
        s.scheduleShift("Manager"); // Because unscheduled/available shifts are always taken by the manager
        return this.scheduled.remove(s);
    }

    // Assigns a shift and returns true if successful
    public Boolean assignShift(Shift s) {
        if (!this.isAvailableFor(s)) { Log.i("Location: ", "AssignShift/first line"); return false;  }
        if (!this.removeAvailableShift(s)) { Log.i("Location: ", "AssignShift/second line"); return false; }
        if (!this.addScheduledShift(s)) {
            Log.i("Location: ", "AssignShift/third line"); this.addAvailableShift(s);
            return false;
        }
        return true;
    }

    public void setTraining(Training t) {
        this.training = t;
    }

    public Training getTraining() {
        return this.training;
    }

    public int getEmployee_id() {
        return Employee_id;
    }

    //this should only be used once
    public void setEmployee_id(int employee_id) {
        Employee_id = employee_id;
    }

    public ContactInfo getContactInfo(){
        return this.contactInfo;
    }

    public void setContactInfo(ContactInfo c){
        this.contactInfo = c;
    }

    public String getEmployeeName(){
        return employeeName;
    }

    public void setEmployeeName(String name){
        employeeName = name;
    }

    public List<Shift> getEmployeeAvailability() { return this.availability; }

    public List<Shift> getScheduled(){ return scheduled; }

    //this should only be called when initialized.
    public void initializeEmployeeShifts(List<Shift> available, List<Shift> schedule){
        setAvailability(available);
        setScheduled(schedule);
    }

    private void setAvailability(List<Shift> availability){
        this.availability = availability;
    }

    private void setScheduled(List<Shift> scheduled){
        this.scheduled = scheduled;
    }

    /* When provided a date, returns true if the employee is not scheduled for a shift the
    same week as the date provided. */
    public Boolean isPriority(Date date) {
        // Get the day of the month for this date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        // Get the week that this day is in (/7)
        int week = dayOfMonth/7;
        // If the employee is scheduled for a shift this week return false (not a priority)
        for (Shift shift : scheduled) {
            if (shift.getWeekDate() == week) {
                return false;
            }
        }
        // This employee is a priority
        return true;
    }
}
