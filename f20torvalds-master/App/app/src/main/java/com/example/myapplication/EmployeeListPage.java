package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class EmployeeListPage extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employee_list);

        refresh();

        // set backwards navigation
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button button = (Button) findViewById(R.id.button);

        // set the button to transition to the add employee
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddEmployee.class);
                v.getContext().startActivity(intent);

            }
        });

    }
    public void onResume(){
        super.onResume();
        refresh();
    }
    public void edit_this(View view){
        ListView items = (ListView) findViewById(R.id.employee_list_View);
        LinearLayout vwParentRow = (LinearLayout) view.getParent();

        TextView child = (TextView) vwParentRow.getChildAt(0);


        String text = (String) child.getText();

        EmployeeManager Employees = EmployeeManager.getInstance();
        List<Employee> temp = Employees.getEmployees();

        //find employee
        for(int i = 0; i < temp.size(); i++){
            if(temp.get(i).getEmployeeName().equals(text)) {
                // pass employee information to edit activity and start edit activity

                Intent intent = new Intent(this, EditEmployee.class);
                intent.putExtra("employee", temp.get(i).toString());
                startActivity(intent);
            }
        }
    }

    public void delete_this(View view){
        ListView items = (ListView) findViewById(R.id.employee_list_View);
        LinearLayout vwParentRow = (LinearLayout) view.getParent();

        TextView child = (TextView) vwParentRow.getChildAt(0);
        Button btnChild = (Button) vwParentRow.getChildAt(1);

        String text = (String) child.getText();

        EmployeeManager Employees = EmployeeManager.getInstance();
        List<Employee> temp = Employees.getEmployees();

        for(int i = 0; i < temp.size(); i++){
            if(temp.get(i).getEmployeeName().equals(text)) {
                Toast.makeText(view.getContext(), "employee id is "+temp.get(i).getEmployee_id(), Toast.LENGTH_SHORT).show();
                Employees.RemoveEmployee(temp.get(i));
            }
        }
        refresh();

    }

    public void edit_availability(View view){
        ListView items = (ListView) findViewById(R.id.employee_list_View);
        LinearLayout vwParentRow = (LinearLayout) view.getParent();

        TextView child = (TextView) vwParentRow.getChildAt(0);


        String text = (String) child.getText();

        EmployeeManager Employees = EmployeeManager.getInstance();
        List<Employee> temp = Employees.getEmployees();

        //find employee
        for(int i = 0; i < temp.size(); i++){
            if(temp.get(i).getEmployeeName().equals(text)) {
                // pass employee information to edit activity and start edit activity

                Intent intent = new Intent(this, AvailabilityEditor.class);
                intent.putExtra("employee", temp.get(i).toString());
                startActivity(intent);
            }
        }
    }

    public void refresh (){
        //initialize Employee manager
        EmployeeManager Employees = EmployeeManager.getInstance();

        ListView EmployeeList = (ListView) findViewById(R.id.employee_list_View);
        //Create Name List
        ArrayList<String> display = new ArrayList<String>();

        List<Employee> temp = Employees.getEmployees();

        for(int i = 0; i < temp.size(); i++){
            String display_string =  "";
            display_string += (temp.get(i).getEmployeeName() + "\n");
            display_string += "Number: " + temp.get(i).getContactInfo().getCel() + "\n";
            display_string += "Email: " + temp.get(i).getContactInfo().getEmail();
            display.add(temp.get(i).getEmployeeName());
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.item_student, R.id.student_name, display);
        EmployeeList.setAdapter(adapter);
    }

}
