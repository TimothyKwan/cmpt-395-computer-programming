package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AvailabilityEditorWeekly extends Activity {

    public SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.CANADA);
    public int day;
    public ShiftManager shiftManager = ShiftManager.getInstance();
    public List<Shift> available;
    public ArrayList<weekday> dayList;
    public String date;
    public String text;
    public Employee employee;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.availability_weekly);

        // set backwards navigation and toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        //get employee name and date from previous activity
        Bundle extras = getIntent().getExtras();
        text = extras.getString("employee");
        date = extras.getString("date");
        day = extras.getInt("dayOfWeek");
        Log.i("employee text: ", text);

        //Find Employee by string and set fields
        EmployeeManager Employees = EmployeeManager.getInstance();
        List<Employee> temp = Employees.getEmployees();


        String name = "";

        // Get employee information
        for(int i = 0; i < temp.size(); i++){
            if(temp.get(i).toString().equals(text)) {
                employee = temp.get(i);
                name = temp.get(i).getEmployeeName();

            }
        }




        //get available shifts for the employee in question
        available = shiftManager.getAvailableShifts(name);
        Log.i("Available: ", available.toString());

        // find textviews
        TextView employeeName = findViewById(R.id.textView);
        TextView firstDayOfWeek = findViewById(R.id.textView3);
        TextView opening = findViewById(R.id.textView2);

        // set textviews based on information passed form outside of activity
        opening.setText("Week starting on " + intToDayOfWeek(day) + ": ");
        firstDayOfWeek.setText(date);
        employeeName.setText(name);

//        Log.i("Date Selected: ", date);
//        Log.i("Name: ", name);

        //find save button textview
        Button save = findViewById(R.id.save_week_Availability);

        // build a list of 7 weekdays with preset toggles
//        dayList = buildDayList(day, date, available);
        refresh();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveWeek();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });



    }
    public void saveWeek() throws ParseException {
        boolean morning;
        boolean afternoon;
        boolean flag = false;

        List<Shift> availability = employee.getEmployeeAvailability();

        ListView list = findViewById(R.id.availability_listView);

        for(int i= 0; i < 7; i++){
            Log.i("i: ", String.valueOf(i));
            View v = list.getAdapter().getView(i, null, null);
            Switch morn = v.findViewById(R.id.morning);
            Switch aft = v.findViewById(R.id.afternoon);
            morning = morn.isChecked();
            afternoon = aft.isChecked();

            Log.i("morning: ", String.valueOf(morning));
            Log.i("afternoon: ", String.valueOf(afternoon));


            // attempt to find a shift with matching date
            for(int j=0; j < availability.size(); j++){

                if(formatter.format(availability.get(j).getDate()).equals(dayList.get(i).getDate())){
                    Log.i("Shift found? ", "True");
                    Shift s = availability.get(j);
                    flag = true;
                    if(!morning && !afternoon){
                        shiftManager.removeShift(s);
                        employee.removeAvailableShift(s);
                        break;
                    }
                    else if (morning && !afternoon){
                        s.setAvailability(Shift.ShiftType.Morning);
                        shiftManager.editShift(s);
                        break;
                    }
                    else if (!morning && afternoon){
                        s.setAvailability(Shift.ShiftType.Afternoon);
                        shiftManager.editShift(s);
                        break;
                    }
                    else {
                        s.setAvailability(Shift.ShiftType.FullDay);
                        shiftManager.editShift(s);
                        break;
                    }

                }
            }
            // if no existing shift is found, add shift
            if((morning || afternoon) && !flag) {
                Shift.ShiftType type = Shift.ShiftType.FullDay;
                if(morning && afternoon){
                    type = Shift.ShiftType.FullDay;
                }
                else if(morning){
                    type = Shift.ShiftType.Morning;
                }
                else {
                    type = Shift.ShiftType.Afternoon;
                }

                Shift s = new Shift(formatter.parse(date), employee.getEmployeeName(), type);
                employee.addAvailableShift(s);
                shiftManager.addShift(s);
                Log.i("Shift: ", "added");
            }


            flag = false;
        }

    }


    // Helper method to convert an integer value to the day of the week
    public String intToDayOfWeek (int i){
        i = i % 7;
        switch (i) {
            case 0:
                return "Saturday ";

            case 1:
                return "Sunday   ";

            case 2:
                return "Monday   ";

            case 3:
                return "Tuesday  ";

            case 4:
                return "Wednesday";

            case 5:
                return "Thursday ";

            case 6:
                return "Friday   ";

            default:
                return "Error    ";


        }
    }
    public void onResume(){
        super.onResume();


        refresh();
    }
    private ArrayList<weekday> buildDayList (int day, String date, List<Shift> avail) {
        ArrayList<weekday> weekList = new ArrayList<>();

//        Log.i("Location: ", "Inside buildDayLIst");
//        Log.i("Avail size: ", String.valueOf(avail.size()));
//        Log.i("date given: ", date);
//        Log.i("Avail contents: ", avail.toString());

//        iterate through the next 7 days, create a weekday object for each value
        for (int i = 0; i < 7; i++) {
            boolean morningChecked = false;
            boolean afternoonChecked = false;

            for (int j = 0; j < avail.size(); j++) {
//                Log.i("Value of j: ", String.valueOf(j));
//                Log.i("formatter", formatter.format(avail.get(j).getDate()));
//                Log.i("Date", date);

                if (formatter.format(avail.get(j).getDate()).equals(date)) {
//                    Log.i("Avail: ", formatter.format(avail.get(j).getDate()));
//                    Log.i("Shift type: ", avail.get(j).getShiftType().toString());
                    if (avail.get(j).getShiftType().equals(Shift.ShiftType.Afternoon)) {
                        afternoonChecked = true;
                    }
                    if (avail.get(j).getShiftType().equals(Shift.ShiftType.Morning)) {
                        morningChecked = true;
                    }
                    if (avail.get(j).getShiftType().equals(Shift.ShiftType.FullDay)) {
                        afternoonChecked = true;
                        morningChecked = true;
                    }

                }
            }


            weekList.add(i, new weekday(intToDayOfWeek(day + i), morningChecked, afternoonChecked, date));

            // increment date
            Date convertedDate = null;
            try {
                convertedDate = formatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(convertedDate);
            calendar.add(Calendar.DATE, 1);
            date = formatter.format(calendar.getTime());



        }
        return weekList;
    }

    public void refresh(){
        dayList = buildDayList(day, date, available);

        ListView weekDays = findViewById(R.id.availability_listView);
//        ArrayList<String> display = new ArrayList<>();

//        for(int i = 0; i < 7; i++){
//            if (((day+i) % 7) == 0){
//                display.add(intToDayOfWeek(day+i));
//            } else{
//             display.add(intToDayOfWeek((day + i) % 7 ));
//            }
//        }

        availabilityAdapter adapter = new availabilityAdapter(this, dayList);
        weekDays.setAdapter(adapter);
    }
}

//weekday class is needed for custom adapter
class weekday {
    private String day;
    private Boolean morning;
    private Boolean afternoon;
    private String date;

    public weekday(String day, Boolean morning, Boolean afternoon, String date){
        this.day = day;
        this.morning = morning;
        this.afternoon = afternoon;
        this.date = date;
    }
    public String getDate() {
        return this.date;
    }

    public String getDay() {
        return this.day;
    }
    public Boolean getMorning () {
        return this.morning;
    }
    public Boolean getAfternoon () {
        return this.afternoon;
    }
}
class availabilityAdapter extends ArrayAdapter<weekday> {
    public availabilityAdapter(Context context, ArrayList<weekday> days){
        super(context, 0, days);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get information about the item at this position
        weekday temp = getItem(position);

        // Check for existing view being resused, otherwise inflate view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.availability_layout, parent, false);
        }

        //lookup relavant view
        TextView dayText = convertView.findViewById(R.id.Weekday_name);
        Switch morning = convertView.findViewById(R.id.morning);
        Switch afternoon = convertView.findViewById(R.id.afternoon);

        // Set data into template
        dayText.setText(temp.getDay());
        morning.setChecked(temp.getMorning());
        afternoon.setChecked(temp.getAfternoon());


        return convertView;

    }
}
