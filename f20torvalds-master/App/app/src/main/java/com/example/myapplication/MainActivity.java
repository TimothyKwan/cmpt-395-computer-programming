package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
    public String selectedDate;
    private Boolean isClicked = false;

//    private Animation rotateOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_open_anim);
//    private Animation rotateClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_close_anim);
//    private Animation fromBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.from_bottom_anim);
//    private Animation toBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.to_bottom_anim);

    public FloatingActionButton expandFab;
    public FloatingActionButton employeeFab;
    public FloatingActionButton calendarFab;
    public FloatingActionButton busyFab;
    public TextView morning;
    public TextView afternoon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize Employee manager
        EmployeeManager Employees = EmployeeManager.getInstance();
        Employees.Initialize(MainActivity.this);
        BusyDayManager.getInstance().initialize(MainActivity.this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //initialize busy days
        final BusyDayManager busyDays = BusyDayManager.getInstance();

        // fab id find, used later for
        this.expandFab = findViewById(R.id.expandFab);
        this.employeeFab = findViewById(R.id.employeeFab);
        this.calendarFab = findViewById(R.id.scheduleFab);
        this.busyFab = findViewById(R.id.busyDays);

        employeeFab.setVisibility(View.INVISIBLE);
        calendarFab.setVisibility(View.INVISIBLE);


        // calendar reference and initial date
        final CalendarView simpleCalendarView = (CalendarView) findViewById(R.id.simpleCalendarView); // get the reference of CalendarView
        selectedDate = formatter.format(simpleCalendarView.getDate());




        // Set listener for button/switch combo
        Button saveButton = (Button) findViewById(R.id.button2);
        final Switch busy = (Switch) findViewById(R.id.switch2);

        //associate public textviews morning and afternoon with the appropriate text views
        morning = findViewById(R.id.openingShiftStaff);
        afternoon = findViewById(R.id.closingShiftStaff);



        //check to see if the initialDate is on the busy list, set switch to true if true
        if (busyDays.isBusy(selectedDate)) {
            busy.setChecked(true);
        }

        // date change listener
        simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                                                       @Override
                                                       public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                                                           // the -1900 is a quirk of the onSelectedDayChange function
                                                           Date tempDate = new Date((year - 1900), month, dayOfMonth);
                                                           selectedDate = formatter.format(tempDate);

                                                           // if dateString matches a string on the busy day list, set switch to true
                                                           busy.setChecked(busyDays.isBusy(selectedDate));
                /*
                if(busyDays.isBusy(selectedDate)){
                    busy.setChecked(true);
                } else {
                    busy.setChecked(false);}

                 */
                                                           Log.i("Location", "Before setText");
                                                           //when date changes set the employee lists to the correct employees
                                                           morning.setText(getEmployeesScheduledbyDate(tempDate, 0));
                                                           afternoon.setText(getEmployeesScheduledbyDate(tempDate, 1));
                }
            }
        );
//        onclick listener for expand button, changes the visibility of the other buttons
        expandFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisible(isClicked);
//                setAnimation(isClicked);
                setClickable(isClicked);
                isClicked = !isClicked;

            }
        });
//        onclick listener for save button, is attached to busy days
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the busy date is checked
                if (busy.isChecked()) {
                    //add current date to busy list if not in list
                    if (!busyDays.isBusy(selectedDate)) {
                        try {
                            busyDays.addBusyDay(selectedDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    //attempt to remove busy day from list
                    if (busyDays.isBusy(selectedDate)) {
                        busyDays.removeBusyDay(selectedDate);
                    }
                }
            }
        });


        // set the floating action button to transition to employeeListPage
        employeeFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EmployeeListPage.class);
                v.getContext().startActivity(intent);
            }
        });

        // set the fab to listen for the calendar activity
        calendarFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ScheduleBuilderActivity.class);
                v.getContext().startActivity(intent);
            }
        });


    }

    private void setVisible(Boolean isClicked) {
        if (!isClicked) {
            employeeFab.setVisibility(View.VISIBLE);
            calendarFab.setVisibility(View.VISIBLE);
            busyFab.setVisibility(View.VISIBLE);
        } else {
            employeeFab.setVisibility(View.INVISIBLE);
            calendarFab.setVisibility(View.INVISIBLE);
            busyFab.setVisibility(View.INVISIBLE);
        }
    }

//    private void setAnimation(Boolean isClicked) {
//        if (!isClicked) {
//            employeeFab.startAnimation(fromBottom);
//            calendarFab.startAnimation(fromBottom);
//            expandFab.startAnimation(rotateOpen);
//        } else {
//            employeeFab.startAnimation(toBottom);
//            calendarFab.startAnimation(toBottom);
//            expandFab.startAnimation(rotateClose);
//        }
//
//    }


    private void setClickable(Boolean isClicked) {
        if (!isClicked) {
            employeeFab.setClickable(true);
            calendarFab.setClickable(true);
            busyFab.setClickable(true);
        } else {
            employeeFab.setClickable(false);
            calendarFab.setClickable(false);
            busyFab.setClickable(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //no inspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public String getEmployeesScheduledbyDate(Date date, int choice) {

        Log.i("Location: ", "Inside getEmployeesScheduledbyDate");
        Log.i("Choice: ", String.valueOf(choice));

        ShiftManager shiftManager = ShiftManager.getInstance();
        List<Shift> allShifts = shiftManager.getShiftByDate(date, true);

        List<Shift> selectedShifts = new ArrayList<>();
        //if choice == 0, get morning shifts
        if (choice == 0) {
            for (int i = 0; i < allShifts.size(); i++) {
                if (allShifts.get(i).getShiftType() == Shift.ShiftType.Morning || allShifts.get(i).getShiftType() == Shift.ShiftType.FullDay) {
                    selectedShifts.add(allShifts.get(i));
                }
            }
        }

        Log.i("selectedShifts", selectedShifts.toString());
        //if choice == 1, get afternoon shifts for named employee
        if (choice == 1) {
            for (int i = 0; i < allShifts.size(); i++) {
                if (allShifts.get(i).getShiftType() == Shift.ShiftType.Afternoon || allShifts.get(i).getShiftType() == Shift.ShiftType.FullDay) {
                    selectedShifts.add(allShifts.get(i));
                }
            }
        }

        // Convert selectedShifts to a string composed of names
        StringBuilder names = new StringBuilder();
        if(!selectedShifts.isEmpty()) {
            for (int i = 0; i < selectedShifts.size(); i++) {
                names.append(selectedShifts.get(i).getName());
                names.append("\n");
            }
        } else {
            return "No one Scheduled";
        }


        return names.toString();


    }
}
