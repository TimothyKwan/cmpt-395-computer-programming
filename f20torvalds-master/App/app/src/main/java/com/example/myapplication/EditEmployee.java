package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import java.util.List;

public class EditEmployee extends Activity {

    public Employee employee = null;

    private EmployeeManager employeeManager = EmployeeManager.getInstance();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.edit_employee);

        Button edit_Employee_button = (Button) findViewById(R.id.edit_employee);
        final EditText name_input = (EditText) findViewById(R.id.Name_input);
        final EditText phone_number = (EditText) findViewById(R.id.textview_phoneNumber);
        final EditText email = (EditText) findViewById(R.id.textview_Email);
        final Switch dayTraining = (Switch) findViewById(R.id.sw_dayTraining);
        final Switch afternoonTraining = (Switch) findViewById(R.id.sw_AfternoonTraining);
        final TextView feedback = (TextView) findViewById(R.id.feedback);

        //Get Employee string
        Intent intent = getIntent();
        String text = intent.getStringExtra("employee");

        //Find Employee by string and set fields
        //EmployeeManager Employees = EmployeeManager.getInstance();
        List<Employee> temp = employeeManager.getEmployees();

        String name = null;
        ContactInfo contact = null;
        Training training = null;


        // Get employee information
        for(int i = 0; i < temp.size(); i++){
            if(temp.get(i).toString().equals(text)) {
                employee = temp.get(i);
                name = temp.get(i).getEmployeeName();
                contact = temp.get(i).getContactInfo();
                training = temp.get(i).getTraining();
            }
        }
        //set employee information
        name_input.setText(name);
        assert contact != null;
        phone_number.setText(contact.getCel());
        email.setText(contact.getEmail());
        dayTraining.setChecked(training.getTrainedToOpen());
        afternoonTraining.setChecked(training.getTrainedToClose());

        edit_Employee_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name =  name_input.getText().toString();
                String number = phone_number.getText().toString();
                String emailAddress = email.getText().toString();


                if(!name.equals("") && !number.equals("") && !emailAddress.equals("")) {
                    // get text from textbox and set employee values
                    employee.setEmployeeName(name_input.getText().toString());
                    ContactInfo c = new ContactInfo(phone_number.getText().toString(), email.getText().toString());
                    employee.setContactInfo(c);
                    boolean dayTrain = dayTraining.isChecked();
                    boolean afternoonTrain = afternoonTraining.isChecked();
                    Training tr = new Training(dayTrain, afternoonTrain);
                    employee.setTraining(tr);
                    Toast.makeText(v.getContext(), "Change is saved " +employeeManager.EditEmployee(employee), Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(v.getContext(), "Invalid input", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // set backwards navigation
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


}
