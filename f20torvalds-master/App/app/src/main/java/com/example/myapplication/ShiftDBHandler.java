package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/*
* purpose: This class handles shift database. It takes parameter from ShiftManager class and add, remove, or edit the database
* Author: Timothy Kwan
* date: 11/22/2020
*
* Public variable
* SHIFT_TABLE: store the shift table name
* SHIFT_ID: store the row ID of shift table
* SHIFT_OWNER: store who the shift belongs to
* SHIFT_PERIOD: store if the shift i morning, afternoon, or full day shift
* SHIFT_PURPOSE: store if the shift is used as a availability or a proper shift
* */
public class ShiftDBHandler extends SQLiteOpenHelper implements DataBaseHandler {
    public static final String SHIFT_TABLE = " SHIFT_TABLE ";
    public static final String SHIFT_ID = " ID ";
    public static final String SHIFT_DATE = " SHIFT_DATE ";
    public static final String SHIFT_OWNER = " SHIFT_OWNER";
    public static final String SHIFT_PERIOD = " SHIFT_PERIOD ";
    public static final String SHIFT_PURPOSE = " SHIFT_PURPOSE ";
    public ShiftDBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /*
    * Author: Timothy Kwan
    * purpose: called when the object is created
    * Date: 11/22/2020
    * */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableStatement = " CREATE TABLE "+ SHIFT_TABLE + " ( " + SHIFT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                SHIFT_DATE + " INTEGER, " +
                SHIFT_OWNER + " TEXT, " +
                SHIFT_PERIOD + " INT, " +
                SHIFT_PURPOSE+ " INT ) ";
        db.execSQL(createTableStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /*
     * Author: Timothy Kwan
     * purpose: Return a list of the employee available shift objects
     * Date: 11/22/2020
     * */
    public List<Shift> getEmployeeAvailableShifts(String name){
        List<Shift> referenceList = new ArrayList<>();
        String queryString = " SELECT * FROM " +
                SHIFT_TABLE + " WHERE "+SHIFT_OWNER + " = " + "'"+name+"'"+
                " AND "+ SHIFT_PURPOSE + " = 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(0);
                Date date = new Date(cursor.getLong(1));
                String employeeName = cursor.getString(2);
                Shift.ShiftType type = intToShiftType(cursor.getInt(3));
                Shift s = new Shift(date, employeeName, type);
                s.setShiftID(id);
                s.setIsAvailableType(true);
                referenceList.add(s);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return referenceList;
    }


    /*
     * Author: Timothy Kwan
     * purpose: return a list of actual shift from the employee
     * Date: 11/22/2020
     * */
    public List<Shift> getEmployeeShifts(String name){
        List<Shift> referenceList = new ArrayList<>();
        String queryString = " SELECT * FROM " +
                SHIFT_TABLE + " WHERE "+SHIFT_OWNER + " = " + "'"+name+"'"+
                " AND "+ SHIFT_PURPOSE + " = 0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(0);
                Date date = new Date(cursor.getLong(1));
                String employeeName = cursor.getString(2);
                Shift.ShiftType type = intToShiftType(cursor.getInt(3));
                Shift s = new Shift(date, employeeName, type);
                s.setShiftID(id);
                s.setIsAvailableType(false);
                referenceList.add(s);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return referenceList;
    }

    /*
     * Author: Timothy Kwan
     * purpose: return a list of Shift objects based on the given date
     * true is for availability
     * false is for scheduled
     * Date: 11/22/2020
     * */
    public List<Shift> getShiftsByDate(Date date, int isAvailable){
        List<Shift> referenceList = new ArrayList<>();
        String queryString = " SELECT * FROM "+
                SHIFT_TABLE+" WHERE "+SHIFT_DATE+" = "+ " "+date.getTime() + " AND "+
                SHIFT_PURPOSE+ " = "+isAvailable;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        if(cursor.moveToFirst()){
            do {
                int id = cursor.getInt(0);
                Date shiftDate = new Date(cursor.getLong(1));
                String employeeName = cursor.getString(2);
                Shift.ShiftType type = intToShiftType(cursor.getInt(3));
                boolean isAvailableType = cursor.getInt(4)==1;
                Shift s = new Shift(shiftDate, employeeName, type);
                s.setShiftID(id);
                s.setIsAvailableType(isAvailableType);
                referenceList.add(s);
            }
            while (cursor.moveToNext());
        }
        return referenceList;
    }
    /*
     * Author: Timothy Kwan
     * purpose: add the given shift into the database. return a positive number when success
     * Date: 11/22/2020
     * */
    public long addShift(Shift s){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SHIFT_DATE, s.getDate().getTime());
        cv.put(SHIFT_OWNER, s.getName());
        cv.put(SHIFT_PERIOD, shiftTypeToInt(s.getShiftType()));
        cv.put(SHIFT_PURPOSE, s.getIsAvailableType()? 1:0);
        long returnMessage = db.insert(SHIFT_TABLE, null, cv);
        s.setShiftID(Integer.parseInt(Long.toString( returnMessage)));
        return returnMessage;
    }

    /*
     * Author: Timothy Kwan
     * purpose: remove the given shift from the database. return true if success
     * Date: 11/22/2020
     * */
    public boolean removeShift(Shift s){
        SQLiteDatabase db = this.getWritableDatabase();
        String queryString = " DELETE FROM "+ SHIFT_TABLE +" WHERE "+SHIFT_ID+ " = " + s.getShiftID();
        Cursor cursor = db.rawQuery(queryString, null);
        return cursor.moveToFirst();
    }

    /*
     * Author: Timothy Kwan
     * purpose: edit the shift base on the given shift. return a positive number if success
     * Date: 11/22/2020
     * */
    public int editShift(Shift s){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SHIFT_DATE, s.getDate().getTime());
        cv.put(SHIFT_OWNER, s.getName());
        cv.put(SHIFT_PERIOD, s.getShiftType()==null? 0:shiftTypeToInt(s.getShiftType()));
        cv.put(SHIFT_PURPOSE, s.getIsAvailableType()? 1:0);
        return db.update(SHIFT_TABLE,cv, SHIFT_ID+" = "+s.getShiftID(), null);
    }

    /*
     * Author: Timothy Kwan
     * purpose: helper method to convert an int into ShiftType enum
     * Date: 11/22/2020
     * */
    private Shift.ShiftType intToShiftType(int num){
        switch (num){
            case 1:
                return Shift.ShiftType.Morning;
            case 2:
                return Shift.ShiftType.Afternoon;
            case 3:
                return Shift.ShiftType.FullDay;
            default:
                return null;
        }
    }

    /*
     * Author: Timothy Kwan
     * purpose: helper method to convert a ShiftType enum into an int
     * Date: 11/22/2020
     * */
    private int shiftTypeToInt(Shift.ShiftType type){
        switch (type){
            case Morning:
                return 1;
            case Afternoon:
                return 2;
            case FullDay:
                return 3;
            default:
                return 0;
        }
    }
}
