package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

//Purpose: Helper class for handling database input and output
//Author: Timothy Kwan
//Date: 10/12/2020
public class EmployeeDBHandler extends SQLiteOpenHelper implements DataBaseHandler{
    private Context ct;
    public static final String EMPLOYEE_TABLE = " EMPLOYEE_TABLE ";
    public static final String ID = "ID";
    public static final String EMPLOYEE_NAME = "EMPLOYEE_NAME";
    public static final String EMPLOYEE_PHONE_NUMBER = "EMPLOYEE_PHONE_NUMBER";
    public static final String EMPLOYEE_EMAIL = "EMPLOYEE_EMAIL";
    public static final String EMPLOYEE_DAY_SHIFT_TRAINING = "EMPLOYEE_DAY_SHIFT_TRAINING";
    public static final String EMPLOYEE_AFTERNOON_SHIFT_TRAINING = "EMPLOYEE_AFTERNOON_SHIFT_TRAINING";

    public EmployeeDBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {

        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableStatement = " CREATE TABLE " + EMPLOYEE_TABLE + " ( " + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                EMPLOYEE_NAME + " TEXT, " +
                EMPLOYEE_PHONE_NUMBER + " TEXT, " +
                EMPLOYEE_EMAIL + " TEXT, " +
                EMPLOYEE_DAY_SHIFT_TRAINING + " INT, " +
                EMPLOYEE_AFTERNOON_SHIFT_TRAINING + " INT )";
        db.execSQL(createTableStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 2){
            db.execSQL("DROP TABLE IF EXISTS "+EMPLOYEE_TABLE);
            onCreate(db);
        }
    }

    public List<Employee> getAllEmployee_db(){
        List<Employee> referenceList = new ArrayList<>();
        String queryString = "SELECT * FROM "+EMPLOYEE_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(queryString, null);
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String phoneNum = cursor.getString(2);
                String email = cursor.getString(3);
                boolean dayShiftTraining = cursor.getInt(4) == 1;
                boolean afternoonShiftTraining = cursor.getInt(5)==1;
                Training tr = new Training(dayShiftTraining,afternoonShiftTraining);
                ContactInfo c = new ContactInfo( phoneNum, email);
                Employee e = new Employee(name, tr, c);
                e.setEmployee_id(id);
                referenceList.add(e);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return referenceList;
    }

//handles the addItem and return if the insert success
    public long addItem(Employee employee) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        ContactInfo ci = employee.getContactInfo();
        Training tr = employee.getTraining();
        //input all the data into the corresponding column
        cv.put(EMPLOYEE_NAME, employee.getEmployeeName());
        cv.put(EMPLOYEE_PHONE_NUMBER, ci.getCel());
        cv.put(EMPLOYEE_EMAIL, ci.getEmail());
        cv.put(EMPLOYEE_DAY_SHIFT_TRAINING, tr.getTrainedToOpen());
        cv.put(EMPLOYEE_AFTERNOON_SHIFT_TRAINING, tr.getTrainedToClose());
        long returnMessage = db.insert(EMPLOYEE_TABLE,null, cv);
        employee.setEmployee_id(Integer.parseInt(Long.toString( returnMessage)));
        //db.close();
        return returnMessage;
    }

    public boolean removeItem(Employee employee) {
        SQLiteDatabase db = this.getWritableDatabase();
        //search item base on the item's id
        String queryString = " DELETE FROM "+ EMPLOYEE_TABLE +" WHERE "+ID+ " = " + employee.getEmployee_id();
        //String queryString = " DELETE FROM "+ EMPLOYEE_TABLE +" WHERE "+ID+ " < " + " 10 ";
        Cursor cursor = db.rawQuery(queryString, null);
        //db.close();
        //cursor.close();
        return cursor.moveToFirst();
    }

    //change the employee data in the database from the input employee
    public int editItem(Employee e){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(EMPLOYEE_NAME, e.getEmployeeName());
        cv.put(EMPLOYEE_PHONE_NUMBER, e.getContactInfo().getCel());
        cv.put(EMPLOYEE_EMAIL, e.getContactInfo().getEmail());
        cv.put(EMPLOYEE_DAY_SHIFT_TRAINING, e.getTraining().getTrainedToOpen()? 1:0);
        cv.put(EMPLOYEE_AFTERNOON_SHIFT_TRAINING, e.getTraining().getTrainedToClose()? 1:0);
        return db.update(EMPLOYEE_TABLE, cv, ID+" = "+e.getEmployee_id(), null);
    }
}
