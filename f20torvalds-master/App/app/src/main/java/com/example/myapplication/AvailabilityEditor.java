package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
/* purpose: handle the availability page for the staffs
*
* =================================================
* Update: removed extra lines. change the setting of the
*/

public class AvailabilityEditor extends Activity {
    public Employee employee = null;
    public List <Shift> availability;
    public Date selectedDate;
    public SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
    public String name = null;
    public int dayOfWeek;
    public String text;

    private ShiftManager shiftManager = ShiftManager.getInstance();

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_availability);

        final CalendarView simpleCalendarView = (CalendarView) findViewById(R.id.simpleCalendarView); // get the reference of CalendarView
        selectedDate = new Date(simpleCalendarView.getDate()); // get selected date in milliseconds
        Calendar calendar = Calendar.getInstance();
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

//        Log.i("First selected Date", formatter.format(selectedDate));

        // set backwards navigation
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView name_text = (TextView) findViewById(R.id.textView);

        Button save = (Button) findViewById(R.id.save_Availability);


        //Get Employee string
        Intent intent = getIntent();
        text = intent.getStringExtra("employee");

        //Find Employee by string and set fields
        EmployeeManager Employees = EmployeeManager.getInstance();
        List<Employee> temp = Employees.getEmployees();

        // Get employee information
        for(int i = 0; i < temp.size(); i++){
            if(temp.get(i).toString().equals(text)) {
                employee = temp.get(i);
                name = temp.get(i).getEmployeeName();

            }
        }
        name_text.setText(name);
        //get switch references
        final Switch dayAvailable = findViewById(R.id.Morning);
        final Switch afternoonAvailable = findViewById(R.id.Afternoon);

        //get shift list for initialization
        availability = employee.getEmployeeAvailability();
        Shift.ShiftType shiftType;

        // search for matching shift, if it exists, set toggles
        for(int i = 0; i < availability.size(); i++){
//            Log.i("First", formatter.format(availability.get(i).getDate()));
//            Log.i("Second", formatter.format(selectedDate));
            if(formatter.format(availability.get(i).getDate()).equals(formatter.format(selectedDate))){
                Log.i("Inside first if", "true");

                shiftType = availability.get(i).getShiftType();
                // if morning shift
                Log.i("Shift type", "" +shiftType==null? "null": shiftType.toString());
                switch(shiftType){
                    case Morning:
                        dayAvailable.setChecked(true);
                        afternoonAvailable.setChecked(false);
                        break;
                    case Afternoon:
                        dayAvailable.setChecked(false);
                        afternoonAvailable.setChecked(true);
                        break;
                    case FullDay:
                        Log.i("AAAAHHHH", "true");
                        dayAvailable.setChecked(true);
                        afternoonAvailable.setChecked(true);
                        break;
                    default:
                        dayAvailable.setChecked(false);
                        afternoonAvailable.setChecked(false);
                        break;
                }
                /*
                if(shiftType == Shift.ShiftType.Morning){
                    dayAvailable.setChecked(true);
                }
                // if evening shift
                else if(shiftType == Shift.ShiftType.Afternoon) {
                    afternoonAvailable.setChecked(true);

                }
                else {
                    dayAvailable.setChecked(true);
                    afternoonAvailable.setChecked(true);
                }*/
            }
        }

        //behaviour of the save button
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                availability = employee.getEmployeeAvailability();

                // search for matching shift, if it exists, set toggles
                for (int i = 0; i < availability.size(); i++) {
//                    Log.i("Values compared:", (formatter.format(availability.get(i).getDate()))+ ' ' + formatter.format(selectedDate));

                    if (formatter.format(availability.get(i).getDate()).equals(formatter.format(selectedDate)))
                    {//change the setting of the shift if the shift exist
                        Shift s = availability.get(i);
                        if(s!=null){
                            if(!dayAvailable.isChecked() && !afternoonAvailable.isChecked()){
                                shiftManager.removeShift(s);
                                availability.remove(s);

                            }else{
                                s.setAvailability(shiftConverter(dayAvailable.isChecked(), afternoonAvailable.isChecked()));
                                //true = availability shift
                                s.setIsAvailableType(true);
                                shiftManager.editShift(s);
                            }
                        }
                        /*
                        Log.i("Pass success", "pass");
                        if(s.getShiftType()==null){
                            shiftManager.removeShift(s);
                            employee.removeAvailableShift(s);
                        }else{
                        }*/
                        return;
                    }
                }
                Log.i("dayAvailable: ", String.valueOf(dayAvailable.isChecked()));
                Log.i("afternoonAvailable: ", String.valueOf(afternoonAvailable.isChecked()));
                if(dayAvailable.isChecked() || afternoonAvailable.isChecked()){
                    Shift.ShiftType shiftType = shiftConverter(dayAvailable.isChecked(), afternoonAvailable.isChecked());
                    Shift newShift = new Shift(selectedDate, name, shiftType);
                    newShift.setIsAvailableType(true);
                    employee.addAvailableShift(newShift);
                    shiftManager.addShift(newShift);
                }

            }
        });

        simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                // the -1900 is a quirk of the onSelectedDayChange function
                selectedDate = new Date((year - 1900), month, dayOfMonth);

                //Weirdly enough, CalendarView cannot seem to get the day of the week, below is the
                // code to get the day of the week from Calendar
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);
                dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);


                availability = employee.getEmployeeAvailability();
                Shift.ShiftType shiftType;



                // search for matching shift, if it exists, set toggles
                for(int i = 0; i < availability.size(); i++){
//                    Log.i("Values compared:", (formatter.format(availability.get(i).getDate()))+ ' ' + formatter.format(selectedDate));

                    if(formatter.format(availability.get(i).getDate()).equals(formatter.format(selectedDate))){
//                        Log.i("match", "match");

                        shiftType = availability.get(i).getShiftType();
//                        Log.i("Shift type ", shiftType==null? "null":shiftType.toString());
                        if(shiftType!=null){
                            switch (shiftType) {
                                case Morning:
                                    dayAvailable.setChecked(true);
                                    afternoonAvailable.setChecked(false);
                                    return;
                                case Afternoon:
                                    afternoonAvailable.setChecked(true);
                                    dayAvailable.setChecked(false);
                                    return;
                                case FullDay:
                                    dayAvailable.setChecked(true);
                                    afternoonAvailable.setChecked(true);
                                    return;
                                default:
                                    dayAvailable.setChecked(false);
                                    afternoonAvailable.setChecked(false);
                                    break;
                            }
                        }
                    }
                }
                dayAvailable.setChecked(false);
                afternoonAvailable.setChecked(false);

            }
        });

        //get id for week view button
        Button week = findViewById(R.id.week_view);

        week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AvailabilityEditorWeekly.class);
                intent.putExtra("employee", employee.toString());
                intent.putExtra("dayOfWeek", dayOfWeek);
                intent.putExtra("date", formatter.format(selectedDate));

                startActivity(intent);
            }
        });
    }



    // helper method to convert booleans from the switches into shiftType enum
    private static Shift.ShiftType shiftConverter(boolean day, boolean afternoon){
        int count = 0;
        if(day){
            count+=1;
        }
        if (afternoon){
            count+=2;
        }
        switch (count){
            case 1:
                return Shift.ShiftType.Morning;
            case 2:
                return Shift.ShiftType.Afternoon;
            case 3:
                return Shift.ShiftType.FullDay;
        }
        return null;
    }


 }

