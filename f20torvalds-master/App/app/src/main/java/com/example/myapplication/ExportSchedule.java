package com.example.myapplication;

/*
 * Creator: Kierra Andruko
 * Date: 29-NOV-2020
 * Last Update: 10-Dec-2020
 * Purpose: to export the list of shifts (both assigned to an employee and to the manager -
 * unassigned) to a text file labeled with the month and year of the shifts.
 */

import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ExportSchedule {
    private List<Shift> employeeShifts;
    private List<Shift> managerShifts; // The unscheduled shifts (availability)

    public ExportSchedule(List<Shift> employees, List<Shift> managerShifts) {
        this.employeeShifts = employees;
        this.managerShifts = managerShifts;

        export(); // Writes all of the shifts to a file labelled with the month and year.
    }

    // Returns the string of the month the shifts are scheduled for
    private String getMonth() {
        Date tempDate = employeeShifts.get(0).getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tempDate);
        int month = calendar.get(Calendar.MONTH);
        switch (month){
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            default:
                return "December";
        }
    }

    // Returns the string of the year
    private String getYear() {
        Date tempDate = employeeShifts.get(0).getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tempDate);
        int year = calendar.get(Calendar.YEAR);
        return Integer.toString(year);
    }

    // Returns the ordered combination of shifts both scheduled for employees, and scheduled for
    // the manager
    private List<Shift> totalShifts() {
        List<Shift> allShifts = new ArrayList<>();
        allShifts.addAll(managerShifts);
        allShifts.addAll(employeeShifts);
        Collections.sort(allShifts);
        return allShifts;
    }

    // Writes the current schedule to a text file with the month and year in the title.
    // Overwrites the file if it already exists (the schedule needed editing).
    public void export() {
        try {
            Log.i("Inside export function", "The try has been entered");
            String fileName = "Schedule" + getMonth() + getYear() + ".txt";
            Log.i("FileName: ", fileName);
            BufferedWriter writer;
            writer = new BufferedWriter(new FileWriter(fileName));
            for (Shift shift : totalShifts()) {
                writer.append("\n");
                writer.append(shift.toString());
                writer.append("\n");
            }
            writer.flush();
            writer.close();
        } catch (Exception e){
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
