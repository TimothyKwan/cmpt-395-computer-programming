package com.example.myapplication;

import java.util.Calendar;
import java.util.Date;

//purpose: Store the shift variable and handle if the employee is available for the shift.
/*
* Private variables
* shiftID: store the id of the shift for database purpose
* date: store the date of the shift
* name: store who this shift belongs to
* shiftPeriod: store if the shift is a morning, afternoon, or full day shift
* isAvailableType: store if this shift is used as to check availability for the employee or a proper shift object
* */
public class Shift implements Comparable<Shift> {
    private int shiftID;
    private Date date;
    private String name;
    private ShiftType shiftPeriod;
    private boolean isAvailableType;

    public Shift(Date date, String name, ShiftType shiftType) {
        this.date = date;
        this.name = name;
        this.shiftPeriod = shiftType;
    }

    // get date
    public Date getDate(){ return date; }

    // Return the week that this shift is dated for
    public int getWeekDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        return dayOfMonth/7;
    }

    // return shift type
    public ShiftType getShiftType() { return shiftPeriod; }

    // set availability
    public void setAvailability(ShiftType type) {
        shiftPeriod = type;
    }

    // comparison
    public Boolean isEqual(Shift s) {
        /*if (!this.date.equals(s.date)) {
            return false;
        }*/
        return (date.equals(s.date) && shiftPeriod == s.shiftPeriod);
    }

    public Boolean isTrained(Training t) {
        if (shiftPeriod == ShiftType.Morning && !t.getTrainedToOpen()) {
            return false;
        }
        if (shiftPeriod == ShiftType.Afternoon && !t.getTrainedToClose()) {
            return false;
        }
        return true;
    }

    // Changes the name data element in the shift to the name of whoever is scheduled to work it.
    public void scheduleShift(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    /* isAvailableType: true means it is for the availability. False means it is scheduled*/
    public void setIsAvailableType(boolean type){
        isAvailableType = type;
    }

    public boolean getIsAvailableType(){
        return isAvailableType;
    }

    public void setShiftID(int id){shiftID=id;}

    public int getShiftID(){return shiftID;}

    @Override
    public String toString() {
        String shiftInfo = "Date: " + this.date +
                "\nEmployee: " + this.name +
                "\nShift time: ";
        switch (shiftPeriod){
            case Morning:
                shiftInfo += "Morning";
                break;
            case Afternoon:
                shiftInfo += "Afternoon";
                break;
            default:
                shiftInfo += "All day";
                break;
        }
        if (this.isAvailableType) {
            shiftInfo += "\nShift type: available";
        } else {
            shiftInfo += "\nShift type: scheduled";
        }
        /*
        if (this.openingShift && this.closingShift) {
            shiftInfo += "All day";
        } else if (this.openingShift && !this.closingShift) {
            shiftInfo += "Morning";
        } else if (!this.openingShift && this.closingShift) {
            shiftInfo += "Afternoon";
        }*/
        return shiftInfo;
    }

    public enum ShiftType{
        Morning,
        Afternoon,
        FullDay
    }

    //@Override
    public int compareTo(Shift s) {
        if (this.getDate().after(s.getDate())) {
            return 1;
        }

        if (this.getDate().before(s.getDate())) {
            return -1;
        }
        return 0; // They must be equal
    }
}