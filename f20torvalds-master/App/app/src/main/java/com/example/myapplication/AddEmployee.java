package com.example.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

public class AddEmployee extends Activity {
    EmployeeManager employeeManager;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_employee);

        Button addEmployee = (Button) findViewById(R.id.add_employee);
        final EditText name_input = (EditText) findViewById(R.id.Name_input);
        final EditText phone_number = (EditText) findViewById(R.id.textview_phoneNumber);
        final EditText email = (EditText) findViewById(R.id.textview_Email);
        final Switch dayTraining = (Switch) findViewById(R.id.sw_dayTraining);
        final Switch afternoonTraining = (Switch) findViewById(R.id.sw_AfternoonTraining);
        final TextView feedback = (TextView) findViewById(R.id.feedback);
        employeeManager = EmployeeManager.getInstance();


        addEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get text from textbox
                String name = name_input.getText().toString();
                String phoneNum = phone_number.getText().toString();
                String emails = email.getText().toString();
                boolean dayTrain = dayTraining.isChecked();
                boolean afternoonTrain = afternoonTraining.isChecked();
                if(!name.equals("") && !phone_number.equals("") && !emails.equals("")) {
                    // display indicator
                    feedback.setText(R.string.EmployeeAddFeedback);

                    // add employee
                    //create new employee
                    Training tr = new Training(dayTrain, afternoonTrain);
                    ContactInfo c = new ContactInfo(phoneNum, emails);
                    Employee new_Employee = new Employee(name, tr, c);

                    //send employee somewhere
                    long num = employeeManager.AddEmployee(new_Employee);
                    Toast.makeText(v.getContext(), "return is "+num, Toast.LENGTH_SHORT).show();
//                    feedback.setText(Employees.getEmployees().get(0).toString());
                }else{
                    Toast.makeText(v.getContext(), "Invalid input", Toast.LENGTH_SHORT).show();
                }

                //set fields to blank string to prevent duplicate adds
                name_input.setText("");
                phone_number.setText("");
                email.setText("");
                dayTraining.setChecked(false);
                afternoonTraining.setChecked(false);
            }
        });
        // set backwards navigation
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
