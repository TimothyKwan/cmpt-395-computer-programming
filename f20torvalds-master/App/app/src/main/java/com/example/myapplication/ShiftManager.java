package com.example.myapplication;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*purpose: This class holds a database handler and commutes with the database handler. This class also handles employee shifts removal
* Author: Timothy Kwan (TK)
* Date: 11/21/2020
*
* Private variables
* isInitialized: check if this is initialized to avoid being initialized again
* shiftList: temporary reference of one employee's shift list
* availableList: temporary reference of one employee's available list
* shift_db: store the name of the shift database
* dbHandler: reference of the database handler
* */
public class ShiftManager {
    private boolean isInitialized = false;
    private List<Shift> shiftList = new ArrayList<>();
    private List<Shift> availableList = new ArrayList<>();
    private String shift_db = "Shift.db";
    private ShiftDBHandler dbHandler;

    //start of singleton
    private static ShiftManager instance = new ShiftManager();

    private ShiftManager() {
    }

    public static ShiftManager getInstance() {
        return instance;
    }
    //end of singleton

    /*
     * Author: Timothy Kwan
     * purpose: initialize the Manager class
     * date: 11/22/2020
     * */
    public void Initialization(Context context) {
        if (!isInitialized) {
            dbHandler = new ShiftDBHandler(context, shift_db, null, 1);
            isInitialized = true;
        }
    }

    /*
     * Author: Timothy Kwan
     * purpose: return a list of shifts from the given name
     * date: 11/22/2020
     * */
    public List<Shift> getEmployeeShifts(String name) {
        return dbHandler.getEmployeeShifts(name);
    }

    /*
     * Author: Timothy Kwan
     * purpose: return a list of available shift from the given name
     * date: 11/22/2020
     * */
    public List<Shift> getAvailableShifts(String name) {
        return dbHandler.getEmployeeAvailableShifts(name);
    }

    /*
     * Author: Timothy Kwan
     * purpose: return a list of Shift objects based on the date given.
     * The identifier are:
     * true is for the availability
     * false is for the scheduled
     * date: 11/25/2020
     * */
    public List<Shift> getShiftByDate(Date date, boolean isAvailable){
        return dbHandler.getShiftsByDate(date, isAvailable? 1:0);
    }
    /*
     * Author: Timothy Kwan
     * purpose: add the given Shift object into the database
     * date: 11/22/2020
     * */
    public long addShift(Shift shift) {
        return dbHandler.addShift(shift);
    }

    /*
     * Author: Timothy Kwan
     * purpose: remove the given Shift object by calling the database handler
     * date: 11/22/2020
     * */
    public boolean removeShift(Shift shift) {
        return dbHandler.removeShift(shift);
    }

    /*
     * Author: Timothy Kwan
     * purpose: this method will remove all employee's shift based on the given name
     * date: 11/22/2020
     * */
    public void removeAllEmployeeShifts(String name) {
        shiftList = getEmployeeShifts(name);
        availableList = getAvailableShifts(name);
        for (int i = 0; i < shiftList.size(); i++) {
            removeShift(shiftList.get(i));
        }
        for (int i = 0; i < availableList.size(); i++) {
            removeShift(availableList.get(i));
        }
    }

    /*
     * Author: Timothy Kwan
     * purpose: this method edit the given shift
     * date: 11/22/2020
     * */
    public int editShift(Shift shift) {
        return dbHandler.editShift(shift);
    }

    /*
     * Author: Timothy Kwan
     * purpose: this method edit all the Shifts base on the given employee's name
     * date: 11/23/2020
     * */
    public void editAllEmployeeShifts(String name) {
        shiftList = getEmployeeShifts(name);
        availableList = getAvailableShifts(name);
        for (int i = 0; i < shiftList.size(); i++) {
            editShift(shiftList.get(i));
        }
        for (int i = 0; i < availableList.size(); i++) {
            editShift(availableList.get(i));
        }
    }
}


