package com.example.myapplication;

/* Author: Kierra Andruko
 *  Date: 11/27/2020 (last updated) */

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ScheduleBuilder {

    private List<Shift> available;
    private List<Shift> scheduled = new ArrayList<>();
    private List<Employee> employees;

    public ScheduleBuilder() {
    }

    /* Automatic schedule builder: Builds a schedule for the current employees */
    public ScheduleBuilder(List<Shift> available, List<Employee> employees) {
        this.available = available; // Treated as the managers schedule
        this.employees = employees;

        Log.i("Location: ", "Inside ScheduleBuilder");
        Log.i("Availability list: ", String.valueOf(available));
        Log.i("Employees", String.valueOf(employees));

        autoSchedule(); // perform an initial scheduling pass
    }

    public void autoSchedule() {
        singlePassSchedule(true); // Everyone has at least one shift scheduled per week
        singlePassSchedule(false); // The rest of the shifts are scheduled
    }

    private void singlePassSchedule(Boolean priority) {
//        Log.i("Locaton: ", "Inside singlePassSchedule");
        for (int nShift = 0; nShift < this.available.size(); nShift++) {
            Log.i("number of shifts", String.valueOf(nShift));
            for (int nEmployee = 0; nEmployee < this.employees.size(); nEmployee++) {
                // If the employee is trained for the shift
                Log.i("nEmployee: ", String.valueOf(nEmployee));
                if (this.available.get(nShift).isTrained(this.employees.get(nEmployee).getTraining())) {
//                    Log.i("Location: ", "Inside training loop");
                    // If the employee has not been scheduled in the last week (priority)
                    if (((priority) && (isPriority(employees.get(nEmployee), available.get(nShift).getDate()))) || (!priority)) {
                        // attempt to assign this shift to this employee
//                        Log.i("Location: ", "Inside Priority Check");
                        if (this.employees.get(nEmployee).assignShift(this.available.get(nShift))) {
                            // we have now assigned this shift
                            Log.i("Location:", "Shift Assigned");
                            // Make sure the shift has the name of who it is assigned too
                            this.available.get(nShift).scheduleShift(this.employees.get(nEmployee).getEmployeeName());
                            // Add the shift to the scheduled shift list
                            this.scheduled.add(this.available.get(nShift));
                            // Remove the shift from the available list
                            this.available.remove(this.available.get(nShift));
                            nShift--; // Because the index changes when something is removed from a list
                            // move on to the next shift
                            // Note: We are setting nEmployee to the max value to mimic the effect of a break for the inner loop
                            nEmployee = this.employees.size();
                        } // else { the shift has not been assigned
                    }
                }
            }
        }
    }

    // Returns true if the employee does not have a shift scheduled in the last week, false otherwise
    public Boolean isPriority(Employee employee, Date date) {
        return employee.isPriority(date);
    }

    // Add and remove functions for manual schedule adjustment
    public Boolean forceSchedule(Employee e, Shift s) {
        // make the employee available
        if (!e.isAvailableFor(s)){
            e.addAvailableShift(s);
        }
        if (e.assignShift(s)) { // Should always return true
            this.available.remove(s);
            s.scheduleShift(e.getEmployeeName());
            return true;
        }
        return false;
    }

    public Employee findEmployee(String employeeName) {
        for (int i = 0; i < employees.size(); i++) {
            if (employees.get(i).getEmployeeName().equalsIgnoreCase(employeeName)){
                return employees.get(i);
            }
        }
        return null; // Employee does not exist
    }

    // Does not yet remove the shift from the employees list of scheduled shifts.
    public void deschedule(Shift s) {
        // Take this shift out of the employees scheduled list
        Employee descheduledEmployee = findEmployee(s.getName());
        descheduledEmployee.removeScheduledShift(s);
        this.available.add(s);
    }

    public List<Shift> getScheduled() {
        return this.scheduled;
    }

    public List<Shift> getAvailable() {
        return this.available;
    }

    public List<Employee> getEmployees() {
        return this.employees;
    }

    // Could take busy days as a parameter
    public List<Shift> defaultAvailableShifts(Date givenDate) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(givenDate);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

            int month = calendar.get(Calendar.MONTH);
            List<Shift> toBeScheduled = new ArrayList<>();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = formatter.parse("2010-" + month + "-00");
            // Get the end date (for loop stops before the first day of the next month)
            int nextMonth;
            if (month == 12) {
                nextMonth = 0;
            } else {
                nextMonth = month++;
            }
            Date endDate = formatter.parse("2010-" + nextMonth + "-00");

            Calendar start = Calendar.getInstance();
            start.setTime(startDate);
            Calendar end = Calendar.getInstance();
            end.setTime(endDate);

            for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
                // Add 2 morning shifts on that day to the list
                Shift morningShift1 = new Shift(date, "manager", Shift.ShiftType.Morning);
                Shift morningShift2 = new Shift(date, "manager", Shift.ShiftType.Morning);
                // Add 2 evening shifts on that day to the list
                Shift eveningShift1 = new Shift(date, "manager", Shift.ShiftType.Afternoon);
                Shift eveningShift2 = new Shift(date, "manager", Shift.ShiftType.Afternoon);
                toBeScheduled.add(morningShift1);
                toBeScheduled.add(morningShift2);
                toBeScheduled.add(eveningShift1);
                toBeScheduled.add(eveningShift2);
            }
            Log.i("toBeScheduled: ", toBeScheduled.get(0).toString());
            return toBeScheduled;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("toBeScheduled: ", null);
        return null; //Hopefully we don't get here
    }

}
