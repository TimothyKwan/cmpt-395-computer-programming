package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ScheduleBuilderActivity extends Activity {

    // Not sure if the context should be [this] or [getApplicationContext()]
//    private Animation rotateOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_open_anim);
//    private Animation rotateClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_close_anim);
//    private Animation fromBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.from_bottom_anim);
//    private Animation toBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.to_bottom_anim);

    private Boolean isClicked = false;
    public SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.CANADA);
    public List<Shift> shifts = new ArrayList<>();

    public FloatingActionButton schedule_btn;
    public FloatingActionButton autoScheduler_btn;
    public FloatingActionButton edit_btn;
    public FloatingActionButton export_btn;
    public ScheduleBuilder build;
    public ExportSchedule export;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_schedule);

        // set backwards navigation
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // get shift manager
        ShiftManager shiftM = ShiftManager.getInstance();

//        Log.i("Location: ", "Get avail and employees");
        //Get availability list, .getAvailableShifts needs to be replaced with
        final List<Shift> avail = shiftM.getAvailableShifts("Kierra");
        final List<Shift> schedul = shiftM.getEmployeeShifts("Kierra");
        Log.i("Availability: ", avail.toString());
        //Get employee manager
        EmployeeManager Emanager = EmployeeManager.getInstance();

        // Below is for troubleshooting, remove when shipping
//        for(int i = 0; i < Emanager.getEmployees().size(); i++){
//            List<Shift> avail = shiftM.getAvailableShifts(Emanager.getEmployees().get(i).getEmployeeName());
//            Log.i("Availability: ", avail.toString());
//        }
//
        //Get Employee list
        final List<Employee> employees = Emanager.getEmployees();
//        Log.i("Employees: ", employees.toString());
//        Log.i("Avail: ", String.valueOf(avail));



        this.schedule_btn = findViewById(R.id.schedule_btn);
        this.autoScheduler_btn = findViewById(R.id.auto_schedule_btn);
        this.edit_btn = findViewById(R.id.edit_btn);
        this.export_btn = findViewById(R.id.export_btn);
        /*schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFABOpen){
                    openFABMenu(schedule, autoScheduler, edit);
                }else{
                    closeFABMenu(schedule, autoScheduler, edit);
                }
            }
        });*/

        schedule_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisible(isClicked);
//                setAnimation(isClicked);
                setClickable(isClicked);
                isClicked = !isClicked;
            }
        });
        autoScheduler_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Auto Schedule Button Clicked", Toast.LENGTH_SHORT).show();
                // Populate schedule Builder class with availability and employee lists
                build = new ScheduleBuilder(avail, employees);
            }
        });
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Edit Schedule Button Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        export_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Export Schedule Button Clicked", Toast.LENGTH_SHORT).show();
                // Export the scheduled shift as well as the unscheduled shifts to a text file.
                export = new ExportSchedule(build.getAvailable(), build.getScheduled()); // Takes available and scheduled shifts list
            }
        });
    }

    private void setVisible(Boolean isClicked) {
        if (!isClicked) {
            autoScheduler_btn.setVisibility(View.VISIBLE);
            edit_btn.setVisibility(View.VISIBLE);
            export_btn.setVisibility(View.VISIBLE);
        } else {
            autoScheduler_btn.setVisibility(View.INVISIBLE);
            edit_btn.setVisibility(View.INVISIBLE);
            export_btn.setVisibility(View.INVISIBLE);
        }
    }

//    private void setAnimation(Boolean isClicked) {
//        if (!isClicked) {
//            autoScheduler_btn.startAnimation(fromBottom);
//            edit_btn.startAnimation(fromBottom);
//            schedule_btn.startAnimation(rotateOpen);
//            export_btn.startAnimation(fromBottom);
//        } else {
//            autoScheduler_btn.startAnimation(toBottom);
//            edit_btn.startAnimation(toBottom);
//            schedule_btn.startAnimation(rotateClose);
//            export_btn.startAnimation(toBottom);
//        }
//
//    }

    private void setClickable(Boolean isClicked) {
        if (!isClicked){
            autoScheduler_btn.setClickable(true);
            edit_btn.setClickable(true);
            export_btn.setClickable(true);
        } else {
            autoScheduler_btn.setClickable(false);
            edit_btn.setClickable(false);
            export_btn.setClickable(false);
        }
    }



/*
    private void openFABMenu(FloatingActionButton fab1, FloatingActionButton fab2, FloatingActionButton fab3){
        isFABOpen = true;
        fab1.animate();
        fab2.animate();
        fab3.animate();
    }

    private void closeFABMenu(FloatingActionButton fab1, FloatingActionButton fab2, FloatingActionButton fab3){
        isFABOpen = false;
        fab1.animate();
        fab2.animate();
        fab3.animate();
    }*/

    public void onResume(){
        super.onResume();
        refresh();
    }


    public void refresh() {

        // get shifts
        ShiftManager shiftmanager = ShiftManager.getInstance();

        EmployeeManager employeeManager = EmployeeManager.getInstance();
        List<Employee> employees = employeeManager.getEmployees();
        List<String> names = new ArrayList<>();
        for(int i = 0; i < employees.size(); i++){
            names.add(employees.get(i).getEmployeeName());
        }

        for(int i = 0; i < names.size(); i ++){
            shifts.addAll(shiftmanager.getEmployeeShifts(names.get(i)));
            shifts.addAll(shiftmanager.getAvailableShifts(names.get(i)));
        }
        List<String> display = new ArrayList<>();
        for(int i = 0; i < shifts.size(); i++){
            display.add(shifts.get(i).toString());
        }

        ListView list = findViewById(R.id.shift_ListView);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.shift_layout, R.id.shift_date, display);
//        shiftAdapter adapter = new shiftAdapter(this, (ArrayList<Shift>) shifts);
        list.setAdapter(adapter);
    }
    //    class shiftAdapter extends ArrayAdapter<Shift> {
//        public shiftAdapter(Context context, ArrayList<Shift> shifts) {
//            super(context, 0, shifts);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            // get information about the item at this position
//            Shift temp = getItem(position);
//
//            // Check for existing view being resused, otherwise inflate view
//            if (convertView == null) {
//                convertView = LayoutInflater.from(getContext()).inflate(R.layout.shift_layout, parent, false);
//            }
//
//            //lookup relavant view
//            TextView date = convertView.findViewById(R.id.shift_date);
//            TextView shiftType = convertView.findViewById(R.id.shiftType_text);
//            TextView name = convertView.findViewById(R.id.name_for_shift);
//            TextView shift = convertView.findViewById(R.id.shift_type);
//
//
//            // Set data into template
//            date.setText(formatter.format(temp.getDate()));
//            shiftType.setText(temp.getShiftType().toString());
//            name.setText(temp.getName());
//
//
//            if(temp.getIsAvailableType()){
//                shift.setText("A");
//            } else {
//                shift.setText("S");
//            }
//
//
//            return convertView;
//
//        }
//    }
    public void deleteThisShift(View view) throws ParseException {

        LinearLayout vwParentRow = (LinearLayout) view.getParent();
        TextView child = (TextView) vwParentRow.getChildAt(0);

        String text = (String) child.getText();

        Log.i("Text: ", text);
        Shift targetShift = null;

        for(int i = 0; i < shifts.size(); i++){
            if(shifts.get(i).toString().equals(text)){
                targetShift = shifts.get(i);
            }
        }

        EmployeeManager employeeManager = EmployeeManager.getInstance();
        List<Employee> employees = employeeManager.getEmployees();
        Employee targetEmployee = null;
        for(int i=0; i < employees.size(); i++){
            assert targetShift != null;
            if(targetShift.getName().equals(employees.get(i).getEmployeeName())){
                targetEmployee = employees.get(i);
            }
        }

        assert targetShift != null;
        assert targetEmployee != null;
        targetEmployee.removeScheduledShift(targetShift);
        targetEmployee.removeAvailableShift(targetShift);
        targetEmployee.getEmployeeAvailability().remove(targetShift);
        ShiftManager shiftmanager = ShiftManager.getInstance();
        shiftmanager.removeShift(targetShift);
        shifts.remove(targetShift);

        refresh();


    }
}